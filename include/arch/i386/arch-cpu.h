/* Copyright (C) 2016 Jingrong Chen <cjr@linux.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ARCH_CPU_H
#define _ARCH_CPU_H

#ifndef __ASSEMBLER__

struct cpu {
	uint8_t apicid;					// Local APIC ID
	volatile uint32_t started;		// Has the CPU started?
	int ncli;						// Depth of pushcli nesting.
	int intena;						// Were interrupts enabled before pushcli?

	// This variable point to the local cpu structure.
	struct cpu *cpu;
};

extern struct cpu cpus[NR_CPUS];
extern int ncpu;

/* This is a very tricky technique
 * But very useful.
 * This cpu variable point to the address of
 * local cpu->cpu.
 */
extern struct cpu *cpu asm("%gs:0");
//extern struct proc *proc asm("%gs:4");

#endif /* !__ASSEMBLER__ */

#endif
