/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ARCH_TRAP_H
#define _ARCH_TRAP_H

#ifndef __ASSEMBLER__

struct trapframe {
	uint32_t edi, esi, ebp, old_esp, ebx, edx, ecx, eax;
	uint16_t gs, pad0, fs, pad1, es, pad2, ds, pad3;
	uint32_t irq, err_code;
	uint32_t eip, cs, eflags;
	uint32_t esp;
	uint16_t ss, pad4;
};

#endif	/* !__ASSEMBLER__ */

#endif /* _ARCH_TRAP_H */

