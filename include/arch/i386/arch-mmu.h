/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ARCH_MMU_H
#define _ARCH_MMU_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

/* addresses before and after early MMU mapping */
#define __premap_addr(kva)	(ULCAST(kva) - KERN_BASE)
#define __postmap_addr(pa)	(ULCAST(pa) + KERN_BASE)

#define GDT_ENTRY(n) ((n) << 3)

/* kernel virtual address and physical address conversion */
#define kva2pa(kva)		(ULCAST(kva) - KERN_BASE)
#define pa2kva(pa)		(PTRCAST(pa) + KERN_BASE)

#define PAGE_SHIFT 12
#define PAGE_SIZE (1 << PAGE_SHIFT) // 4KB
#define XPAGE_SHIFT 22
#define XPAGE_SIZE (1 << XPAGE_SHIFT) // 4MB

#define make_xpte(addr) (ALIGN_BELOW((addr), XPAGE_SIZE) | 0x80 | 0x3)
#define make_pte(addr) (ALIGN_BELOW((addr), PAGE_SIZE) | 0x80 | 0x3)

#define CR4_PSE (1 << 4)

/* Refer bellow code from nemu's kernel */

#define SEG_CODEDATA            1
#define SEG_32BIT               1
#define SEG_4KB_GRANULARITY     1
#define SEG_TSS_32BIT           0x9

#define DPL_KERNEL              0
#define DPL_USER                3

#define SEG_WRITABLE            0x2
#define SEG_READABLE            0x2
#define SEG_EXECUTABLE          0x8

#define NR_SEGMENTS             5
#define SEG_NULL                0
#define SEG_KERNEL_CODE         1
#define SEG_KERNEL_DATA         2
#define SEG_USER_CODE         3
#define SEG_USER_DATA         4

#ifndef __ASSEMBLER__

#include <util.h>

typedef uint32_t	pde_t;
typedef uint32_t	pte_t;

typedef pde_t	pgindex_t;

/* the Control Register 0 */
typedef union CR0 {
	struct {
		uint32_t protect_enable      : 1;
		uint32_t monitor_coprocessor : 1;
		uint32_t emulation           : 1;
		uint32_t task_switched       : 1;
		uint32_t extension_type      : 1;
		uint32_t numeric_error       : 1;
		uint32_t pad0                : 10;
		uint32_t write_protect       : 1; 
		uint32_t pad1                : 1; 
		uint32_t alignment_mask      : 1;
		uint32_t pad2                : 10;
		uint32_t no_write_through    : 1;
		uint32_t cache_disable       : 1;
		uint32_t paging              : 1;
	};
	uint32_t val;
} CR0;

/* the Control Register 3 (physical address of page directory) */
typedef union CR3 {
	struct {
		uint32_t pad0                : 3;
		uint32_t page_write_through  : 1;
		uint32_t page_cache_disable  : 1;
		uint32_t pad1                : 7;
		uint32_t page_directory_base : 20;
	};
	uint32_t val;
} CR3;

/* the 64bit segment descriptor */
typedef struct SegmentDescriptor {
    uint32_t limit_15_0          : 16;
    uint32_t base_15_0           : 16;
    uint32_t base_23_16          : 8;
    uint32_t type                : 4;
    uint32_t segment_type        : 1;
    uint32_t privilege_level     : 2;
    uint32_t present             : 1;
    uint32_t limit_19_16         : 4;
    uint32_t soft_use            : 1;
    uint32_t operation_size      : 1;
    uint32_t pad0                : 1;
    uint32_t granularity         : 1;
    uint32_t base_31_24          : 8;
} segdesc_t;

typedef struct GateDescriptor {
    uint32_t offset_15_0      : 16;
    uint32_t segment          : 16;
    uint32_t pad0             : 8;
    uint32_t type             : 4;
    uint32_t system           : 1;
    uint32_t privilege_level  : 2;
    uint32_t present          : 1;
    uint32_t offset_31_16     : 16;
} gatedesc_t;

/* modify the value of GS */
static inline void
write_gs(uint16_t val) {
	asm volatile("movw %0, %%gs" : : "r"(val));
}

/* read the value of GS */
static inline uint16_t
read_gs() {
	uint16_t gs;
	asm volatile("movw %%gs, %0" : : "r"(gs));
	return gs;
}

/* modify the value of GDTR */
static inline void
write_gdtr(void *addr, uint16_t size) {
    static volatile uint16_t data[3];
    data[0] = size - 1;
    data[1] = (uint32_t)addr;
    data[2] = ((uint32_t)addr) >> 16;
    asm volatile("lgdt (%0)" : : "r"(data));
}

/* modify the value of IDTR */
static inline void
write_idtr(void *addr, uint16_t size) {
    static volatile uint16_t data[3];
    data[0] = size - 1;
    data[1] = (uint32_t)addr;
    data[2] = ((uint32_t)addr) >> 16;
    asm volatile("lidt (%0)" : : "r"(data));
}

/* read CR0 */
static inline uint32_t
read_cr0() {
	uint32_t val;
	asm volatile("movl %%cr0, %0" : "=r"(val));
	return val;
}

/* write CR0 */
static inline void
write_cr0(uint32_t cr0) {
	asm volatile("movl %0, %%cr0" : : "r"(cr0));
}

/* write CR3, notice that CR3 is never read */
static inline void
write_cr3(uint32_t cr3) {
	asm volatile("movl %0, %%cr3" : : "r"(cr3));
}

/* read CR4 */
static inline uint32_t
read_cr4() {
	uint32_t val;
	asm volatile("movl %%cr4, %0" : "=r"(val));
	return val;
}

/*write CR4 */
static inline void
write_cr4(uint32_t cr4) {
	asm volatile("movl %0, %%cr4" : : "r"(cr4));
}

struct __attribute__ ((packed, aligned(8))) e820map_t {
	addr_t base_addr;
	long long length;
	int type;
/*
 * 01h    memory, available to OS
 * 02h    reserved, not available (e.g. system ROM, memory-mapped device)
 * 03h    ACPI Reclaim Memory (usable by OS after reading ACPI tables)
 * 04h    ACPI NVS Memory (OS is required to save this memory between NVS sessions)
 * other  not defined yet -- treat as Reserved
 */
#define E820_MEMORY	1
#define E820_RESERVED 2
#define E820_ACPI_RECLAIM_MEM 3
#define E820_ACPI_NVS_MEM 4
#define E820_OTHER 5
} e820map_t;

#endif /* !__ASSEMBLER__ */

#endif /* !_ARCH_MMU_H */

