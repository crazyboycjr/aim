/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ARCH_BOOT_H
#define _ARCH_BOOT_H

#define BAD_TRAP \
	do { \
		asm volatile("cli"); \
		for (;;) asm volatile("hlt"); \
	} while (0)

#ifndef __ASSEMBLER__

// Copy from nemu
#include <sys/types.h>

#define SECTSIZE 512
#define IDE_PORT_BASE 0x1F0

static inline uint8_t
in_byte(uint16_t port) {
	uint8_t data;
	asm volatile("in %1, %0" : "=a"(data) : "d"(port));
	return data;
}

static inline uint32_t
in_long(uint16_t port) {
	uint32_t data;
	asm volatile("in %1, %0" : "=a"(data) : "d"(port));
	return data;
}

static inline void
out_byte(uint16_t port, uint8_t data) {
	asm volatile("out %%al, %%dx" : : "a"(data), "d"(port));
}

static inline void
out_long(uint16_t port, uint32_t data) {
	asm volatile("out %%eax, %%dx" : : "a"(data), "d"(port));
}

static inline uint16_t
in_word(uint16_t port) {
	uint16_t data;
	asm volatile("in %1, %0" : "=a"(data) : "d"(port));
	return data;
}

static inline void
out_word(uint16_t port, uint16_t data) {
	asm volatile("out %%al, %%dx" : : "a"(data), "d"(port));
}

/* memory_set and insl refer to xv6 code in order to compress code */
static inline void
memory_cpy(void *addr, void* data, int cnt) {
	/*asm volatile("cld;"
				 "rep movsb;"
				 : "=D"(addr), "=c"(cnt)
				 : "0"(addr), "1"(cnt), "m" (data)
				 : "memory", "cc");
	*/for (int i = 0; i < cnt; i++)
		*(uint8_t *)(addr + i) = *(uint8_t *)(data + i);
}

static inline void
memory_set(void *addr, int data, int cnt) {
	asm volatile("cld; rep stosb" :
				 "=D" (addr), "=c" (cnt) :
				 "0" (addr), "1" (cnt), "a" (data) :
				 "memory", "cc");
	//for (int i = 0; i < cnt; i++)
	//	*(uint8_t *)(addr + i) = data;
}

void
waitdisk() {
	while ( (in_byte(IDE_PORT_BASE + 7) & (0x80 | 0x40)) != 0x40);
}

void
readsect(void *dst, uint32_t sector) {
	waitdisk();
	out_byte(IDE_PORT_BASE + 1, 0); //Do not use DMA read.

	out_byte(IDE_PORT_BASE + 2, 1); // count = 1
	out_byte(IDE_PORT_BASE + 3, sector);
	out_byte(IDE_PORT_BASE + 4, sector >> 8);
	out_byte(IDE_PORT_BASE + 5, sector >> 16);
	out_byte(IDE_PORT_BASE + 6, 0xE0 | (sector >> 24));
	out_byte(IDE_PORT_BASE + 7, 0x20); //0xc8 is DMA

	waitdisk();
	for (int i = 0; i < SECTSIZE / sizeof(uint32_t); i ++) {
		*(((uint32_t*)dst) + i) = in_long(IDE_PORT_BASE);
	}
}

#endif /* !__ASSEMBLER__ */

#endif /* !_ARCH_BOOT_H */

