/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ARCH_IRQ_H
#define _ARCH_IRQ_H

#ifndef __ASSEMBLER__

static inline void local_irq_enable()
{
	asm volatile("sti" : : : "memory");
}

static inline void local_irq_disable()
{
	/* This "memory" tell compiler not to disorganize the instrs */
	asm volatile("cli" : : : "memory");
}

static inline unsigned long native_save_fl(void)
{
	unsigned long flags;

	asm volatile("# __raw_save_flags\n\t"
				 "pushf ; pop %0"
				 : "=rm" (flags)
				 : /* no input */
				 : "memory");

	return flags;
}

static inline void native_restore_fl(unsigned long flags)
{
	asm volatile("push %0 ; popf"
				 : /* no output */
				 :"g" (flags)
				 :"memory", "cc");
}

#define local_irq_save(flags) \
	do { \
		flags = native_save_fl(); \
		local_irq_disable(); \
	} while (0)

#define local_irq_restore(flags) \
	do { \
		native_restore_fl(flags); \
	} while (0)

#endif /* !__ASSEMBLER__ */

#endif /* _ARCH_IRQ_H */

