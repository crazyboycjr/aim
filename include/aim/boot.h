/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _AIM_BOOT_H
#define _AIM_BOOT_H

#include <arch-boot.h>
#include <sys/types.h>

#ifndef __ASSEMBLER__

// These two structs comes from Linux/block/partitions/efi.h

#ifndef __packed
#define __packed __attribute__((__packed__))
#endif

typedef struct _partition_table {
	uint8_t boot_indicator;
	uint8_t start_head;
	uint8_t start_sector;
	uint8_t start_track;
	uint8_t os_type;
	uint8_t end_head;
	uint8_t end_sector;
	uint8_t end_track;
	uint32_t starting_lba;
	uint32_t size_in_lba;
} __packed partition_table;

typedef struct _legacy_mbr {
	uint8_t boot_code[440];
	uint32_t unique_mbr_signature;
	uint16_t unknown;
	partition_table partition_record[4];
	uint16_t signature;
} __packed legacy_mbr;

extern legacy_mbr mbr;

// read `count` bytes from `offset` of the disk into `pa`
void
ide_read(void *pa, uint32_t offset, uint32_t count)
{
	void *epa;
	epa = pa + count;
	pa -= offset % SECTSIZE;
	offset = (offset / SECTSIZE); 
	for (; pa < epa; pa += SECTSIZE, offset++)
		readsect(pa, offset);
}

#endif /* !__ASSEMBLER__ */

#endif /* !_AIM_BOOT_H */

