/* from internet */
#include <libc/string.h>

void* memmove(void* dest, const void* src, size_t n)
{
	char*     d  = (char*) dest;
	const char*  s = (const char*) src;

	if (s>d)
	{
		 // start at beginning of s
		 while (n--)
			*d++ = *s++;
	}
	else if (s<d)
	{
		// start at end of s
		d = d+n-1;
		s = s+n-1;

		while (n--)
		   *d-- = *s--;
	}
	return dest;
}
