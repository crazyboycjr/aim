/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 * Copyright (C) 2016 Gan Quan <coin2028@hotmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

/* from uart driver */
#include <uart.h>
#include <uart-ns16550-hw.h>
#include <aim/mmu.h>
#include <aim/console.h>
#include <aim/device.h>
#include <aim/initcalls.h>
#include <aim/vmm.h>
#include <platform.h>
#include <aim/gfp.h>
/* from libc */
#include <libc/stdio.h>

#define BUFSIZ	1024 //TODO
TODO(what to do with BUFSIZ 1024)

/* internal routines */

static inline
void __uart_init(struct chr_device *inst)
{
	struct bus_device *bus = inst->bus;
	bus_write_fp bus_write8 = bus->bus_driver.get_write_fp(bus, 8);

	if (bus_write8 == NULL)
		return;		/* should panic? */

	/* TODO: check if the following configuration works across all
	 * UARTs */
	bus_write8(bus, inst->base, UART_FIFO_CONTROL,
		UART_FCR_RTB_4 | UART_FCR_RST_TRANSMIT | UART_FCR_RST_RECEIVER |
		UART_FCR_ENABLE);
	bus_write8(bus, inst->base, UART_LINE_CONTROL, UART_LCR_DLAB);
	bus_write8(bus, inst->base, UART_DIVISOR_LSB,
		(UART_FREQ / UART_BAUDRATE) & 0xff);
	bus_write8(bus, inst->base, UART_DIVISOR_MSB,
		((UART_FREQ / UART_BAUDRATE) >> 8) & 0xff);
	bus_write8(bus, inst->base, UART_LINE_CONTROL,
		UART_LCR_DATA_8BIT |
		UART_LCR_STOP_1BIT |
		UART_LCR_PARITY_NONE);
}

static inline
void __uart_enable(struct chr_device *inst)
{
	struct bus_device *bus = inst->bus;
	bus_write_fp bus_write8 = bus->bus_driver.get_write_fp(bus, 8);

	if (bus_write8 == NULL)
		return;		/* should panic? */

	bus_write8(bus, inst->base, UART_MODEM_CONTROL,
		UART_MCR_RTSC | UART_MCR_DTRC);
}

static inline
void __uart_disable(struct chr_device *inst)
{
	struct bus_device *bus = inst->bus;
	bus_write_fp bus_write8 = bus->bus_driver.get_write_fp(bus, 8);

	if (bus_write8 == NULL)
		return;		/* should panic? */

	bus_write8(bus, inst->base, UART_MODEM_CONTROL, 0);
}

static inline
unsigned char __uart_getchar(struct chr_device *inst)
{
	struct bus_device *bus = inst->bus;
	bus_read_fp bus_read8 = bus->bus_driver.get_read_fp(bus, 8);
	uint64_t buf;

	if (bus_read8 == NULL)
		return 0;		/* should panic? */
	do {
		bus_read8(bus, inst->base, UART_LINE_STATUS, &buf);
	} while (!(buf & UART_LSR_DATA_READY));

	bus_read8(bus, inst->base, UART_RCV_BUFFER, &buf);
	return (unsigned char)buf;
}

static inline
int __uart_putchar(struct chr_device *inst, unsigned char c)
{
	struct bus_device *bus = inst->bus;
	bus_read_fp bus_read8 = bus->bus_driver.get_read_fp(bus, 8);
	bus_write_fp bus_write8 = bus->bus_driver.get_write_fp(bus, 8);
	uint64_t buf;

	if (bus_read8 == NULL || bus_write8 == NULL)
		return EOF;

	do {
		bus_read8(bus, inst->base, UART_LINE_STATUS, &buf);
	} while (!(buf & UART_LSR_THRE));

	bus_write8(bus, inst->base, UART_TRANS_HOLD, c);
	return 0;
}

static inline
int __getc(dev_t dev)
{
	struct chr_device *inst = (struct chr_device *)dev_from_id(dev);
	return __uart_getchar(inst);
}

static inline
int __putc(dev_t dev, int c)
{
	struct chr_device *inst = (struct chr_device *)dev_from_id(dev);
	__uart_putchar(inst, c);
	return 0;
}

/* Meant to register to kernel, so this interface routine is static */
static inline
int console_putchar(int c)
{
	struct chr_device *inst = (struct chr_device *)dev_from_name("uart");
	__uart_putchar(inst, c);
	return 0;
}

/*
int __console_init(struct bus_device *bus, addr_t base, addr_t mapped_base)
{

	set_console(console_putchar, DEFAULT_KPUTS);

	return 0;
}
*/
static struct chr_driver drv;

static struct chr_driver drv = {
	.class = DEVCLASS_CHR,
	.getc = __getc,
	.putc = __putc,
//	.new = __new //TODO
};

static int __driver_init(void)
{
	kputs("KERN: <uart> initializing.\n");

	struct chr_device *uart;
	/* uart_major = 0, uart_devno = 0 */
	register_driver(0, &drv);

	uart = (struct chr_device *)kmalloc(sizeof(*uart), GFP_ZERO);

	uart->bus = (struct bus_device *)dev_from_name("portio");
	uart->base = UART_BASE;

	__uart_init(uart);
	__uart_enable(uart);

	FIXME(devno = 0?)
	initdev(uart, DEVCLASS_CHR, "uart", 0, &drv);
	dev_add(uart);

	set_console(console_putchar, DEFAULT_KPUTS);

	kputs("KERN: <uart> Done.\n");
	return 0;
}

INITCALL_DRIVER(__driver_init);

#ifdef RAW /* baremetal driver */

#else /* not RAW, or kernel driver */

#endif /* RAW */
