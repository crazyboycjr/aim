/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 * Copyright (C) 2016 Jingrong Chen <crazyboycjr@linux.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <sys/param.h>
//#include <aim/sync.h>
#include <aim/mmu.h>
#include <aim/pmm.h>
#include <libc/string.h>
#include <util.h>
#include <aim/vmm.h>
#include <aim/debug.h>

#undef max2
static inline lsize_t max2(lsize_t a, lsize_t b) {
	return a > b ? a : b;
}

typedef struct splay_node splay_node;
static struct __attribute__ ((packed, aligned(4))) splay_node {
	splay_node *fa, *s[2];
	lsize_t max;
	struct pages pages;
} *root;

static lsize_t free_space;

#define NDSZ (sizeof(struct splay_node)) // 40 bytes?

static inline void
new_node(splay_node *node, lsize_t max, struct pages* pages)
{
	*node = (splay_node) {
		.fa = 0, .s = {0, 0},
		.max = max,
		.pages = *pages
	};
}

#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic push
static inline void
set(splay_node *p, splay_node *k, int d)
{
	if (p->s[d] = k)
		k->fa = p;
}
#pragma GCC diagnostic pop

static inline int which(splay_node *node) {
	return node->fa->s[1] == node;
}

static inline lsize_t get_max(splay_node *node) {
	return node ? node->max : 0;
}

static inline addr_t get_paddr(splay_node *node) {
	return node ? node->pages.paddr : 0;
}

static inline bool chk(splay_node *node, lsize_t size) {
	return get_max(node) >= size;
}

static inline void update(splay_node *node) {
	node->max = max2(get_max(node->s[0]), get_max(node->s[1]));
	if (node->max < node->pages.size)
		node->max = node->pages.size;
}

static inline void rotate(splay_node *node) {
	splay_node *y = node->fa;
	int d = which(node);
	set(y, node->s[d ^ 1], d);
	if (!y->fa) node->fa = NULL;
	else set(y->fa, node, which(y));
	set(node, y, d ^ 1);
	update(y);
}

static splay_node *__splay(splay_node **root, splay_node *node, splay_node *goal)
{
	while (node->fa != goal) {
		splay_node *y = node->fa;
		if (y->fa == goal) rotate(node);
		else if (which(node) == which(y)) rotate(y), rotate(node);
		else rotate(node), rotate(node);
	}
	update(node);
	if (!goal) *root = node;
	return node;
}
#define splay(root, node) __splay(root, node, NULL)
#define splay_goal(root, node, goal) __splay(root, node, goal)

/*
 * TODO Change to best fit.
 * XXX Small KSTACKSIZE may cause overflow
 */
TODO(Change to best fit)
static splay_node* splay_query(splay_node **root, lsize_t size) {
	splay_node *node = *root, *p = NULL;
	if (get_max(*root) < size)
		return NULL;

	while (node && node->max >= size) {
		p = node;
		node = node->s[chk(node->s[1], size)];
	}
	assert(p);
	return splay(root, p);
}

static splay_node *splay_insert(splay_node **root, splay_node *newnode)
{
	if (*root == NULL)
		return *root = newnode;

	splay_node *node = *root, *p = NULL;
	while (node) {
		p = node;
		node = node->s[get_paddr(newnode) > node->pages.paddr];
	}
	set(p, newnode, newnode > p);
	return splay(root, newnode);
}

/*
 * If node's prev or succ is NULL,
 * we return node itself.
 */
static splay_node *splay_shift(splay_node **root, splay_node *node, int d)
{
	splay(root, node);
	splay_node *p = node;
	for (node = (*root)->s[d]; node; p = node, node = node->s[d ^ 1]);
	return p;
}

static void splay_join(splay_node **root, splay_node *l, splay_node *r)
{
	assert(l || r);
	if (!(l && r)) {
		*root = PTRCAST(ULCAST(l) ^ ULCAST(r));
		return;
	}
	*root = splay_shift(&l, l, 1);
	set(*root, r, 1);
	update(*root);
}

static void splay_del(splay_node **root, splay_node *node)
{
	splay(root, node);
	if (node->s[0])
		node->s[0]->fa = NULL;
	if (node->s[1])
		node->s[1]->fa = NULL;
	splay_join(root, node->s[0], node->s[1]);
}

static int __alloc(splay_node **root, struct pages *pages)
{
	splay_node *node = splay_query(root, pages->size);

	assert(IS_ALIGNED(pages->size, PAGE_SIZE));

	if (!node) {
		/* lack of page, page fault */
		panic("lack of page, page fault");
		return EOF;
	}

	pages->paddr = node->pages.paddr;
	node->pages.paddr += pages->size;

	assert(node->pages.size >= pages->size);
	node->pages.size -= pages->size;
	splay(root, node);

	free_space -= pages->size;

	assert(node->pages.size >= 0);
	assert(node->pages.size > 0);
	if (node->pages.size == 0) {
		/* del this node */
		splay_del(root, node);
	}
	return 0;
}

static void __free(splay_node **root, struct pages *pages)
{
	splay_node *node = kmalloc(NDSZ, GFP_ZERO);
	new_node(node, pages->size, pages);

	splay_insert(root, node);
	free_space += pages->size;

	/* Merge page */
	splay_node *prev = splay_shift(root, node, 0);
	splay_node *next = splay_shift(root, node, 1);
	/* merge left node to right node */
	if (prev && prev->pages.paddr + prev->pages.size == node->pages.paddr) {
		node->pages.paddr = prev->pages.paddr;
		node->pages.size += prev->pages.size;
		splay_del(root, prev);
	}
	if (next && node->pages.paddr + node->pages.size == next->pages.paddr) {
		next->pages.paddr = node->pages.paddr;
		next->pages.size += node->pages.size;
		splay_del(root, node);
	}

}

static addr_t __get_free(void)
{
	return free_space;
}

static int wrapper_alloc(struct pages *pages)
{
	return __alloc(&root, pages);
}

static void wrapper_free(struct pages *pages)
{
	__free(&root, pages);
}

int page_allocator_init(void)
{
	_Static_assert(NDSZ == 40, "node size not satisfied");
	/*
	 * root will be initialized when calling add_memory_pages
	 * so we do not need to initialize root here.
	 */
	struct page_allocator allocator = {
		.alloc		= wrapper_alloc,
		.free		= wrapper_free,
		.get_free	= __get_free
	};

	set_page_allocator(&allocator);
	return 0;
}

static void pgmove_add_dfs(splay_node *node, splay_node **newroot) {
	if (!node)
		return;

	kpdebug("node: %p %p %p\n  max = %x ", node->fa, node->s[0], node->s[1],
			(uint32_t)node->max);
	print_pages(&node->pages);

	pgmove_add_dfs(node->s[0], newroot);
	pgmove_add_dfs(node->s[1], newroot);

	splay_node *newnode = kmalloc(NDSZ, GFP_ZERO);
	newnode->pages = node->pages;
	newnode->max = node->max;
	splay_insert(newroot, newnode);
}

/*
 * Actually we do not need to remove the space which allocated
 * by `old` because it is on the stack
 */
int page_allocator_move(struct simple_allocator *old)
{
	splay_node *newroot = NULL;
	pgmove_add_dfs(root, &newroot);
	root = newroot;
	return 0;
}
