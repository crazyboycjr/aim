/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 * Copyright (C) 2016 Jingrong Chen <crazyboycjr@linux.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <sys/param.h>
//#include <aim/sync.h>
#include <aim/vmm.h>
#include <aim/mmu.h>	/* PAGE_SIZE */
#include <aim/panic.h>
#include <libc/string.h>
#include <aim/pmm.h>
#include <aim/vmm.h>

#undef max2
static inline int max2(int a, int b) {
	return a > b ? a : b;
}

typedef struct splay_node splay_node;
static struct __attribute__ ((packed, aligned(4))) splay_node {
	splay_node *fa, *s[2];
	gfp_t flags;
	size_t size, max;
	bool free; // a bool is an unsigned int
} *root, *advroot;

#define NDSZ (sizeof(struct splay_node)) // 28

static inline void
new_node(splay_node *node, gfp_t flags, size_t size, size_t max, bool free)
{
	*node = (splay_node) {
		.fa = 0, .s = {0, 0},
		.flags = flags,
		.size = size,
		.max = max,
		.free = free
	};
}

#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic push

static inline void
set(splay_node *p, splay_node *k, int d)
{
	if (p->s[d] = k)
		k->fa = p;
}

#pragma GCC diagnostic pop

static inline void
unset(splay_node *p, int d)
{
	if (p->s[d])
		p->s[d]->fa = NULL;
	p->s[d] = NULL;
}

static inline int which(splay_node *node) {
	assert(node->fa);
	return node->fa->s[1] == node;
}

static inline bool is_free(splay_node *node) {
	return node ? node->free : false;
}

static inline size_t get_max(splay_node *node) {
	return node ? node->max : 0;
}

static inline bool chk(splay_node *node, size_t size) {
	return get_max(node) >= size; // need not chk isfree!
}

static inline void update(splay_node *node) {
	node->max = max2(get_max(node->s[0]), get_max(node->s[1]));
	if (node->free && node->max < node->size)
		node->max = node->size;
}

static inline void rotate(splay_node *node) {
	splay_node *y = node->fa;
	int d = which(node);
	set(y, node->s[d ^ 1], d);
	if (!y->fa) node->fa = NULL;
	else set(y->fa, node, which(y));
	set(node, y, d ^ 1);
	update(y);
}

static splay_node *__splay(splay_node **root, splay_node *node, splay_node *goal)
{
	while (node->fa != goal) {
		splay_node *y = node->fa;
		if (y->fa == goal) rotate(node);
		else if (which(node) == which(y)) rotate(y), rotate(node);
		else rotate(node), rotate(node);
	}
	update(node);
	if (!goal) *root = node;
	return node;
}
#define splay(root, node) __splay(root, node, NULL)
#define splay_goal(root, node, goal) __splay(root, node, goal)

#ifdef DEBUG
static int dfs(splay_node *node) {
	if (!node) return 0;
	int ret = 0;
	ret = max2(ret, dfs(node->s[0]));
	ret = max2(ret, dfs(node->s[1]));
	kpdebug("%p dep = %d\n", node, ret + 1);
	return ret + 1;
}

int print_max_depth(splay_node *node)
{
	int dep = dfs(node);
	kpdebug("depth = %d\n", dep);
	return dep;
}
#endif

/*
 * TODO Change to best fit.
 * XXX with a small KSTACKSIZE, the query function may
 * lead to stack overflow
 */
TODO(Change to best fit)
static splay_node* splay_query(splay_node **root, size_t size) {
	splay_node *node = *root, *p = NULL;
	if (get_max(*root) < size)
		return NULL;

	while (node && node->max >= size) {
		p = node;
		node = node->s[chk(node->s[1], size)];
	}
	assert(p);
	/* !!! forget to splay */
	return splay(root, p);
}

static splay_node* splay_find(splay_node **root, splay_node *obj) {
	splay_node *node = *root, *p = NULL;
	while (node) {
		p = node;
		if (node == obj)
			break;
		node = node->s[obj > node];
	}
	assert(p); // Assert root is not NULL
	return splay(root, p);
}

static splay_node *splay_insert(splay_node **root, splay_node *newnode)
{
	if (*root == NULL)
		return *root = newnode;

	splay_node *node = *root, *p = NULL;
	while (node) {
		p = node;
		node = node->s[newnode > node];
	}
	assert(newnode != p);
	set(p, newnode, newnode > p);
	return splay(root, newnode);
}

/*
 * If node's prev or succ is NULL,
 * we return node itself.
 */
static splay_node *splay_shift(splay_node **root, splay_node *node, int d)
{
	splay(root, node);
	splay_node *p = node;
	for (node = (*root)->s[d]; node; p = node, node = node->s[d ^ 1]);
	return p;
}

static void splay_join(splay_node **root, splay_node *l, splay_node *r)
{
	assert(l || r);
	if (!(l && r)) {
		*root = PTRCAST(ULCAST(l) ^ ULCAST(r));
		return;
	}
	*root = splay_shift(&l, l, 1);
	set(*root, r, 1);
	update(*root);
}

static void splay_del(splay_node **root, splay_node *node)
{
	splay(root, node);
	if (node->s[0])
		node->s[0]->fa = NULL;
	if (node->s[1])
		node->s[1]->fa = NULL;
	splay_join(root, node->s[0], node->s[1]);
}

static void *__simple_alloc(splay_node **root, size_t size, gfp_t flags)
{

	size_t real_size = ROUNDUP(size + NDSZ, NDSZ);
	splay_node *node = splay_query(root, real_size);

	if (!node) {
		/* ask for a new page */
		struct pages pages = {
			.paddr = 0,
			.size = ALIGN_ABOVE(real_size, PAGE_SIZE),
			.flags = flags
		};

		if (alloc_pages(&pages) == EOF) {
			/* page falut */
			return NULL;
		}
		node = (splay_node *)pa2kva(pages.paddr);
		new_node(node, flags, pages.size, pages.size, true);

		splay_insert(root, node);
	}

	/* WTF!!!! */
	/* if (node->size - real_size > NDSZ) */
	assert(node->size >= real_size);
	if (node->size > real_size + NDSZ) {
		/* if equal, the left space is 0 and useless */
		splay_node *newnode = (void *)node + real_size;
		new_node(newnode, node->flags, node->size - real_size,
				 node->size - real_size, true);

		splay_insert(root, newnode);
		node->size = real_size;
	}

	node->free = false;
	splay(root, node);
	return node + 1;
}

static void __simple_free(splay_node **root, void *obj)
{
	obj = (splay_node *)obj - 1;
	splay_node *node = splay_find(root, obj);
	if (!node || node->free)
		panic("simple allocator free error!\n");

	/*
	 * Merge nodes
	 * find prev and next, and try merge.
	 */
	splay_node *prev = splay_shift(root, node, 0);
	splay_node *next = splay_shift(root, node, 1);
	/* merge right node to left node */
	if (next > node && next->free && node + node->size == next) {
		node->size += next->size;
		splay_del(root, next);
	}
	if (prev < node && prev->free && prev + prev->size == node) {
		prev->size += node->size;
		splay_del(root, node);
		node = prev;
	}

	node->free = true;
	splay(root, node);

	/* Return pages */
	if (IS_ALIGNED(ULCAST(node), PAGE_SIZE) && node->size >= PAGE_SIZE) {
		struct pages pages = {
			.paddr = (addr_t)ADDR_CAST(node),
			.size = ALIGN_BELOW(node->size, PAGE_SIZE),
			.flags = node->flags
		};
		assert(node->size >= pages.size);

		/* substitute newnode for node */
		splay_node *newnode = (void *)node + pages.size;
		new_node(newnode, node->flags, node->size - pages.size,
				 node->size - pages.size, true);

		splay_del(root, node);
		if (newnode->size)
			splay_insert(root, newnode);

		free_pages(&pages);
	}
}

static inline size_t __simple_size(void *obj)
{
	splay_node *x = obj;
	return (--x)->size;
}

static void *primary_simple_alloc(size_t size, gfp_t flags)
{
	return __simple_alloc(&root, size, flags);
}

static void *advanced_simple_alloc(size_t size, gfp_t flags)
{
	return __simple_alloc(&advroot, size, flags);
}

static void primary_simple_free(void *obj)
{
	__simple_free(&root, obj);
}

static void advanced_simple_free(void *obj)
{
	__simple_free(&advroot, obj);
}

int simple_allocator_bootstrap(void *pt, size_t size)
{
	_Static_assert(NDSZ == 28 , "node size not satisfied");
	root = pt;
	root->free = true;
	root->max = root->size = size;

	struct simple_allocator primary_allocator = {
		.alloc	= primary_simple_alloc,
		.free	= primary_simple_free,
		.size	= __simple_size
	};

	set_simple_allocator(&primary_allocator);
	return 0;
}

int simple_allocator_init(void)
{
	struct simple_allocator advanced_allocator = {
		.alloc	= advanced_simple_alloc,
		.free	= advanced_simple_free,
		.size	= __simple_size
	};

	set_simple_allocator(&advanced_allocator);
	return 0;
}
