/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <util.h>
#include <sys/types.h>
#include <aim/initcalls.h>
#include <aim/io.h>

int do_one_initcall(initcall_t fn)
{
	return fn();
}


int do_early_initcalls()
{
	return 0;
}

int do_initcalls()
{
	extern uint32_t norm_init_start, norm_init_end;
	uint32_t start = ADDR_CAST(&norm_init_start);
	uint32_t end = ADDR_CAST(&norm_init_end);
	for (uint32_t t, fn = start; fn < end; fn += 4) {
		if ((t = do_one_initcall((initcall_t)read32(fn))) != 0)
			return t;
	}
	return 0;
}
