/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <util.h>
#include <sys/types.h>
#include <aim/console.h>
#include <aim/device.h>
#include <aim/early_kmmap.h>
#include <aim/init.h>
#include <aim/mmu.h>
#include <aim/panic.h>
#include <drivers/io/io-mem.h>
#include <drivers/io/io-port.h>
#include <platform.h>
#include <aim/vmm.h>
#include <aim/pmm.h>
#include <aim/trap.h>
#include <aim/initcalls.h>
#include <aim/smp.h>
#include <aim/proc.h>
#include <aim/sched.h>

extern pgindex_t boot_page_index;

void print_int_helper(long long x) {
	if (x == 0) kputs("0");
	char buf[20];
	int len = 0;
	while (x) {
		int t = x % 16;
		buf[len++] = t <= 9 ? t + '0' : t + 'a' - 10;
		x /= 16;
	}
	while (len--)
		kputchar(buf[len + 1]);
	kputchar('\n');
}

int print_early_mapping_entries() {
	// TODO print my early_mapping table
	struct early_mapping *it = early_mapping_next(NULL);
	for (; it != NULL; it = early_mapping_next(it)) {
		kprintf("paddr = %x, vaddr = %x, size = %x, type = %x\n",
				(uint32_t)it->paddr,
				(uint32_t)ULCAST(it->vaddr),
				(uint32_t)it->size, it->type);
	}
    return 0;
}

static inline
int early_devices_init(void)
{
#ifdef IO_MEM_ROOT
	if (io_mem_init(&early_memory_bus) < 0)
		return EOF;
#endif /* IO_MEM_ROOT */

#ifdef IO_PORT_ROOT
	if (io_port_init(&early_port_bus) < 0)
		return EOF;
#endif /* IO_PORT_ROOT */
	return 0;
}

__noreturn
void master_early_init(void)
{

	/* clear address-space-related callback handlers */
	early_mapping_clear();

	mmu_handlers_clear();
	/* prepare early devices like memory bus and port bus */
	if (early_devices_init() < 0)
		goto panic;

	if (early_console_init(
		EARLY_CONSOLE_BUS,
		EARLY_CONSOLE_BASE,
		EARLY_CONSOLE_MAPPING
	) < 0)
		panic("Early console init failed.\n");

	/* other preperations, including early secondary buses */
    arch_early_init();

	print_early_mapping_entries();
	/* apply my early_mapping */
    if (EOF == page_index_init(PTRCAST(premap_addr(&boot_page_index))))
		goto panic;

	mmu_init(PTRCAST(premap_addr(&boot_page_index)));

	extern uint32_t set_stack;
	abs_jump(&set_stack);

	goto panic;

panic:
	while (1);
}

static void test_kmalloc()
{
	int c = 0x9fc00-27, p = 120, m = 149, o = 7;
	int sum = 0;
	for (int i = 0; i < 100000; i++) {
		sum += c;
		//kpdebug("kmalloc(%d, 0) %d\n", c, sum);
		//assert(kmalloc(c, 0));
		kfree(kmalloc(c, 0));
		c = (c * m + o) % p;
	}
	kpdebug("\033[01;32mPASS \033[0m test_kmalloc()\n");
}

static void test_trap()
{

	asm volatile(R"(
				int $3
				 )");

	asm volatile(R"(
				int $14
				 )");

	asm volatile(R"(
				 pushl %eax
				 pushl %ebx
				 movl $1, %eax
				 xorl %ebx, %ebx
				 int $0x80
				 popl %ebx
				 popl %eax
				 )");

	kpdebug("\033[01;32mPASS \033[0m test_trap()\n");
}

__noreturn
void master_early_init_after(void) {

	init_segment();
	kputs("Hello, world!\n");

#define TEMP_SIZE 2048
	uint8_t temp_stack_space[TEMP_SIZE];
	simple_allocator_bootstrap(&temp_stack_space, TEMP_SIZE);
	kputs("primary simple allocator bootstraped.\n");
	page_allocator_init();
	add_memory_pages();

	simple_allocator_init();
	/*
	 * I don't think we need to pass an old simple_allocatot
	 * to page_allocator_move()
	 */
	page_allocator_move(NULL);

	test_kmalloc();

	trap_init();
	test_trap();

	assert(0 == do_early_initcalls());
	assert(0 == do_initcalls());

	mm_init(); // new pgindex
	sched_init();
	smp_startup();

	//spawn_initproc(); // this should be after initcalls
	proc_init();
	cpu_idle(); // run idle process
	panic("Should not reach here!\n");
}
