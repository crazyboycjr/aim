/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <libc/string.h>
#include <aim/init.h>
#include <aim/early_kmmap.h>
#include <util.h>
#include <aim/mmu.h>
#include <aim/console.h>
#include <aim/debug.h>
#include <aim/cpu.h>
#include <aim/smp.h>

int probe_early_mapping() {
	int num = *(int *)0x8000;
	struct e820map_t *e820map = (void *)0x8004;

	kputs("Print E820MAP:\n");
	kprintf("sizeof e820map_t = 0x%x\n", sizeof(struct e820map_t));

	struct early_mapping desc;
	for (int i = 0; i < num; i++, e820map++) {
		kprintf("base_addr = %x, length = %x, type = %x\n",
				(uint32_t)e820map->base_addr,
				(uint32_t)e820map->length,
				e820map->type);
		if (e820map->type != E820_MEMORY)
			continue;

		desc.type = EARLY_MAPPING_MEMORY;
		desc.paddr = ALIGN_BELOW(e820map->base_addr, XPAGE_SIZE);

		desc.vaddr = PTRCAST(KERN_BASE + desc.paddr);
		desc.size = min2(KMMAP_BASE - ULCAST(desc.vaddr), e820map->length);
		desc.size = ALIGN_ABOVE(desc.size, XPAGE_SIZE);

		if (early_mapping_add(&desc) < 0)
			return EOF;

		desc.vaddr = PTRCAST(desc.paddr);
		if (early_mapping_add(&desc) < 0)
			return EOF;
	}

	/* device mapping address */
	desc.type = EARLY_MAPPING_MEMORY;
	desc.paddr = ALIGN_BELOW(0xfee00000, XPAGE_SIZE);
	desc.vaddr = PTRCAST(desc.paddr);
	desc.size = XPAGE_SIZE;

	if (early_mapping_add(&desc) < 0)
		return EOF;

	kputs("\n");
	return 0;
}

__noreturn
void spin_panic() {
panic:
	goto panic;
}

void arch_early_init(void)
{
	early_mapping_clear();
	if (probe_early_mapping() == EOF)
		spin_panic();
	//asm volatile("cli");
	//for (;;) asm volatile("hlt");
}

static segdesc_t gdt[NR_SEGMENTS + NR_CPUS];

static void
set_segment(segdesc_t *ptr, uint32_t pl, uint32_t type, size_t base) {
    ptr->limit_15_0  = 0xFFFF;
	ptr->base_15_0   = base & 0xffff;
	ptr->base_23_16  = base  >> 16 & 0xff;
    ptr->type = type;
    ptr->segment_type = 1;
    ptr->privilege_level = pl;
    ptr->present = 1;
    ptr->limit_19_16 = 0xF;
    ptr->soft_use = 0;
    ptr->operation_size = 0;
    ptr->pad0 = 1;
    ptr->granularity = 1;
	ptr->base_31_24  = base >> 24;
}

void init_segment(void) {
	memset(gdt, 0, sizeof(gdt));
	set_segment(&gdt[SEG_KERNEL_CODE], DPL_KERNEL, SEG_EXECUTABLE | SEG_READABLE, 0);
	set_segment(&gdt[SEG_KERNEL_DATA], DPL_KERNEL, SEG_WRITABLE, 0);
	//set_segment(&gdt[SEG_USER_CODE], DPL_USER, SEG_EXECUTABLE | SEG_READABLE);
	//set_segment(&gdt[SEG_USER_DATA], DPL_USER, SEG_WRITABLE);

    write_gdtr(gdt, sizeof(gdt));
}

void add_segment(void) {
	int cid = cpuid();
	struct percpu *c = &cpus[cid];
	/* no limit is dangerous */
	set_segment(&gdt[cid + NR_SEGMENTS], DPL_KERNEL,
			SEG_EXECUTABLE | SEG_READABLE | SEG_WRITABLE, ADDR_CAST(&c->cpu));

	write_gdtr(gdt, sizeof(gdt));
	write_gs(GDT_ENTRY(cid + NR_SEGMENTS));
	assert(cid < NR_CPUS);

	/* Tricky!
	 * here we can use
	 * 1. c->cpu = c;
	 * or use
	 * 2. cpu = c;
	 */
	//c->cpu = c;
	cpu = c;
}

__noreturn
void set_stack() {
	extern uint32_t bootstacktop;
    asm volatile ("movl %0, %%esp" : : "r"(&bootstacktop + KSTACKSIZE));

	extern uint32_t master_early_init_after;
	abs_jump(&master_early_init_after);
}

__noreturn
void abs_jump(void *addr) {
	asm volatile ("jmp *%0" : : "r"(addr));
	spin_panic();
}
