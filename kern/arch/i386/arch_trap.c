/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 * Copyright (C) 2016 Jingrong Chen <cjr@linux.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <libc/string.h>
#include <sys/types.h>
#include <aim/mmu.h>
#include <aim/debug.h>
#include <aim/trap.h>
#include <arch-trap.h>

/* refer from nemu */
#define INTR_GATE 0xE
#define TRAP_GATE 0xF

#define IDT_ENTRY(n) ((n) << 3)

#define NR_IRQ 256
static gatedesc_t idt[NR_IRQ];

static void set_gate(gatedesc_t *ptr, uint32_t selector, uint32_t offset,
					 uint32_t dpl, uint32_t type)
{
	*ptr = (gatedesc_t) {
		.offset_15_0 = offset & 0xffff,
		.segment = selector,
		.pad0 = 0,
		.type = type,
		.system = 0,
		.privilege_level = dpl,
		.present = 1,
		.offset_31_16 = (offset >> 16) & 0xffff,
	};
}

void trap_init(void)
{
	extern uint32_t __vecs[]; // .long is 32bits
	for (int i = 0; i < NR_IRQ; i++) {
		set_gate(&idt[i], IDT_ENTRY(SEG_KERNEL_CODE), __vecs[i],
				 DPL_KERNEL, TRAP_GATE);
	}

	set_gate(&idt[0x80], IDT_ENTRY(SEG_KERNEL_CODE), __vecs[0x80],
			DPL_USER, TRAP_GATE);
	write_idtr(idt, sizeof idt);
}

#define SYS_CALL 0x80
#define IRQ_OFFSET 0x20

/* https://www-s.acm.illinois.edu/sigops/2007/roll_your_own/i386/idt.html */
static const char* const excep_name[] = {
	"Divide-by-zero	fault",
	"Debug exception	trap or fault",
	"Non-Maskable Interrupt (NMI)	trap",
	"Breakpoint (INT 3)	trap",
	"Overflow (INTO with EFlags[OF] set)	trap",
	"Bound exception (BOUND on out-of-bounds access)	trap",
	"Invalid Opcode	trap",
	"FPU not available	trap",
	"Double Fault	abort",
	"Coprocessor Segment Overrun	abort",
	"Invalid TSS	fault",
	"Segment not present	fault",
	"Stack exception	fault",
	"General Protection	fault or trap",
	"Page fault	fault",
	"Reserved",
	"Floating-point error	fault",
	"Alignment Check	fault",
	"Machine Check	abort",
};

#define NEXCEP (sizeof(excep_name) / sizeof(excep_name[0]))

void irq_handle(struct trapframe *tf)
{
	int irq = tf->irq;
	kpdebug("irq number is 0x%x\n", irq);
	if (irq < 0) {
		panic("Unhandled exception!");
	} else if (0 <= irq && irq < NEXCEP) {
		kpdebug("%s\n", excep_name[irq]);
		handle_interrupt(irq);
	} else if (NEXCEP <= irq && irq < IRQ_OFFSET) {
		panic("Reserved By Intel\n");
	} else if (IRQ_OFFSET <= irq && irq < 256) {
		if (irq == SYS_CALL) {
			/* i386          ebx   ecx   edx   esi   edi   ebp */
			tf->eax = handle_syscall(tf->eax, tf->ebx, tf->ecx, tf->edx,
									 tf->esi, tf->edi, tf->ebp);
		} else {
			handle_interrupt(irq);
		}
	} else {
		kpdebug("irq number is 0x%x\n", irq);
		panic("Unhandled exception!");
	}

	//abs_jump(trap_return);
	trap_return(tf);
}

__noreturn
void trap_return(struct trapframe *tf)
{
	asm volatile(R"(
				 popl %ebp
				 add $4, %esp
				 popl %esp
				 popal

				 popl %gs
				 popl %fs
				 popl %es
				 popl %ds

				 addl $0x8, %esp
				 iret
				 )");
	while (1);
}
