/* Copyright (C) 2016 Gan Quan <coin2028@hotmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <libc/string.h>
#include <sys/types.h>
#include <aim/proc.h>
#include <aim/percpu.h>
#include <aim/smp.h>
#include <aim/trap.h>
#include <arch-trap.h>
#include <arch-irq.h>
#include <context.h>
#include <aim/sched.h>

extern void forkret(void);
extern void forkrets(void *arg1);
extern void switch_regs(struct context *old, struct context *new);
extern void kernel_thread_entry(void *arg);
uint32_t flags;

void
forkret(void) {
	local_irq_restore(flags);
	forkrets(current_proc->context.esp);
}

int
do_exit(int error_code) {
	//panic("process exit!!.\n");
	assert(current_proc != cpu_idleproc);
	/* destroy current_proc->mm */
	current_proc->state = PS_ZOMBIE;
	current_proc->exit_code = error_code;

	uint32_t flags;
	local_irq_save(flags);
	//assert(cpu_idleproc->state == PS_SLEEPING);
	cpu_idleproc->state = PS_RUNNABLE;
	local_irq_restore(flags);

	schedule();
}

static struct trapframe *__proc_trapframe(struct proc *proc)
{
	struct trapframe *tf;

	tf = (struct trapframe *)(kstacktop(proc) - sizeof(*tf));
	return tf;
}

static void __bootstrap_trapframe(struct trapframe *tf,
				   void *entry,
				   void *stacktop,
				   void *args)
{
	memset(tf, 0, sizeof *tf);
	tf->cs = SEG_KERNEL_CODE << 3;
	tf->ds = tf->es = tf->ss = SEG_KERNEL_DATA << 3;
	tf->ebx = (uint32_t)entry;
	tf->edx = (uint32_t)args;
	tf->eip = (uint32_t)kernel_thread_entry;
	tf->esp = (uint32_t)stacktop;
	tf->eax = 0;
	//tf->eflags |= 0x00000200; //IF
}

static void __bootstrap_context(struct context *context, struct trapframe *tf)
{
	context->eip = (uint32_t)forkret;
	context->esp = (uint32_t)tf;
}

static void __bootstrap_user(struct trapframe *tf)
{
}

void __proc_ksetup(struct proc *proc, void *entry, void *args)
{
	struct trapframe *tf = __proc_trapframe(proc);
	__bootstrap_trapframe(tf, entry, kstacktop(proc), args);
	__bootstrap_context(&(proc->context), tf);
}

void __proc_usetup(struct proc *proc, void *entry, void *stacktop, void *args)
{
	struct trapframe *tf = __proc_trapframe(proc);
	__bootstrap_trapframe(tf, entry, stacktop, args);
	__bootstrap_context(&(proc->context), tf);
	__bootstrap_user(tf);
}

void __prepare_trapframe_and_stack(struct trapframe *tf, void *entry,
    void *ustacktop, int argc, char *argv[], char *envp[])
{
}

void proc_trap_return(struct proc *proc)
{
	struct trapframe *tf = __proc_trapframe(proc);

	trap_return(tf);
}

void __arch_fork(struct proc *child, struct proc *parent)
{
	struct trapframe *tf_child = __proc_trapframe(child);
	struct trapframe *tf_parent = __proc_trapframe(parent);

	*tf_child = *tf_parent;
	/* fill return value here */

	__bootstrap_context(&(child->context), tf_child);
}

void switch_context(struct proc *proc)
{
	struct proc *current = current_proc;
	if (current == proc)
		return;

	local_irq_save(flags);
	current_proc = proc;

	/* Switch page directory */
	switch_pgindex(proc->mm->pgindex);
	/* Switch general registers */
	switch_regs(&(current->context), &(proc->context));
}

