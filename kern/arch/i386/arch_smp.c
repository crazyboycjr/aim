/* LICENCES */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <libc/string.h>
#include <sys/types.h>
#include <aim/debug.h>
#include <aim/mmu.h>
#include <aim/vmm.h>
#include <asm.h>
#include <aim/smp.h>
#include <aim/io.h>
#include <aim/init.h>
#include <aim/trap.h>
#include <arch-mp.h>
//#include <arch-cpu.h>
#include <aim/percpu.h>
#include <aim/proc.h>

extern volatile uint *lapic;  // Initialized in arch_mp.c

void
lapicw(int index, int value)
{
	lapic[index] = value;
	lapic[ID];  // wait for write to finish, by reading
}

void
lapicinit(void)
{
  if(!lapic)
	return;
#define T_SYSCALL       64      // system call
#define T_DEFAULT      500      // catchall

#define T_IRQ0          32      // IRQ 0 corresponds to int T_IRQ

#define IRQ_TIMER        0
#define IRQ_KBD          1
#define IRQ_COM1         4
#define IRQ_IDE         14
#define IRQ_ERROR       19
#define IRQ_SPURIOUS    31


  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
  lapicw(TICR, 10000000);

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
	lapicw(PCINT, MASKED);

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
  lapicw(ESR, 0);

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
	;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
}

#define CMOS_PORT    0x70
#define CMOS_RETURN  0x71

static void
lapicstartap(uchar apicid, uint addr)
{
  int i;
  ushort *wrv;

  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
  microdelay(200);
  lapicw(ICRLO, INIT | LEVEL);
  microdelay(100);    // should be 10ms, but too slow in Bochs!

  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
	lapicw(ICRHI, apicid<<24);
	lapicw(ICRLO, STARTUP | (addr>>12));
	microdelay(200);
  }
}

int nr_cpus(void)
{
	return ncpu;
}

int cpuid(void)
{
	int apicid, i;
#define FL_IF           0x00000200      // Interrupt Enable

	// Cannot call cpu when interrupts are enabled:
	// result not guaranteed to last long enough to be used!
	// Would prefer to panic but even printing is chancy here:
	// almost everything, including kprintf and panic, calls cpu,
	// often indirectly through acquire and release.
	if(readeflags()&FL_IF){
	  static int n;
	  if(n++ == 0)
		kprintf("cpu called from %x with interrupts enabled\n",
		  __builtin_return_address(0));
	}

	if (!lapic)
	  return 0;

	apicid = lapic[ID] >> 24;
	for (i = 0; i < ncpu; ++i) {
	  if (cpus[i].apicid == apicid)
		return i;
	}
	panic("unknown apicid\n");
}

#include <aim/sync.h>
static rlock_t lock = EMPTY_RLOCK(lock);
static semaphore_t sem;

#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic push

static void spinlock_test(void)
{
	while (1) {
		recursive_lock(&lock);
		kprintf("cpu%d: starting\n", cpuid());
		recursive_unlock(&lock);
	}
}

static void semaphore_test(void)
{
	while (1) {
		semaphore_dec(&sem);
		kprintf("cpu%d: starting\n", cpuid());
		semaphore_inc(&sem);
	}
}

#pragma GCC diagnostic pop

void mpmain()
{
	add_segment();
	trap_init();

	lapicinit();
	kprintf("cpu%d: starting\n", cpuid());
	xchg(&cpu->started, 1); // tell startothers() we're up

	//spinlock_test();
	//semaphore_test();
	//proc_init();
	//cpu_idle();

	__local_panic();
}

// Other CPUs jump here from entryother.S.
void
mpenter(void)
{
	init_segment();

	extern pgindex_t boot_page_index;
	mmu_init(PTRCAST(premap_addr(&boot_page_index)));
	asm volatile ("jmp *%0" : : "r"(mpmain));
}

static void
startothers(void)
{
	extern uint8_t __mp_code_start, __mp_code_end;
	extern uint8_t mpgdt;
	void *code;
	struct percpu *c;
	void *stack;

	code = PTRCAST(premap_addr(MP_START));
	uint32_t code_length = ULCAST(&__mp_code_end - &__mp_code_start);
	memcpy(code, &__mp_code_start, code_length);

	assert(premap_addr(code) == MP_START);

	for (c = cpus; c < cpus + nr_cpus(); c++) {
		if (c == cpus + cpuid())  // BSP started already.
			continue;

#define MPSTACKSZ 32768
		/* stack top */
		stack = kmalloc(MPSTACKSZ, GFP_ZERO);
		write32(ULCAST(code - 4), premap_addr(stack + MPSTACKSZ));

		/* six bytes gdtentry */
		write16(ULCAST(code - 10), 23);
		write32(ULCAST(code - 8), ADDR_CAST(&mpgdt));

		/* mp jump entry */
		write32(ULCAST(code - 14), premap_addr(mpenter));
		lapicstartap(c->apicid, premap_addr(code));

		// wait for cpu to finish mpmain()
		while(c->started == 0)
			;
	}
}

void smp_startup(void)
{
	kpdebug("NR_CPUS = %x\n", NR_CPUS);
	mpinit();
	kpdebug("nr_cpus = %x\n", nr_cpus());
	lapicinit();

	kprintf("cpu%d: starting\n", cpuid());
	add_segment();

	semaphore_init(&sem, 3);
	startothers();
}

/* Returns 0 when succeed */
int handle_ipi_interrupt(unsigned int msg);
