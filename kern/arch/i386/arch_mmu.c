/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <libc/string.h>
#include <sys/types.h>
#include <aim/mmu.h>
#include <aim/pmm.h>
#include <aim/console.h>
#include <aim/debug.h>
#include <aim/early_kmmap.h>

void page_index_clear(pgindex_t *index) {
	memset(index, 0, PAGE_SIZE);
}

void mmu_init(pgindex_t *boot_page_index)
{
	CR0 cr0;
	CR3 cr3;
	uint32_t cr4;

	cr4 = read_cr4();
	cr4 |= CR4_PSE;
	write_cr4(cr4);

	cr3.val = ULCAST(boot_page_index);
	write_cr3(cr3.val);

	cr0.val = read_cr0();
	cr0.paging = 1;
	cr0.write_protect = 1;
	write_cr0(cr0.val);
}

int page_index_early_map(pgindex_t *boot_page_index, addr_t paddr,
	void *vaddr, size_t size)
{
	TODO(check some invalid instance and return EOF)

	for (size_t sz = 0; sz < size; sz += XPAGE_SIZE) {
        addr_t va = ULCAST(vaddr) + sz;
		boot_page_index[va >> XPAGE_SHIFT] = make_xpte(paddr + sz);
	}

	return 0;
}

bool early_mapping_valid(struct early_mapping *entry) {
    addr_t va = ULCAST(entry->vaddr);
	if (!(IS_ALIGNED(entry->paddr, XPAGE_SIZE)
			&& IS_ALIGNED(va, XPAGE_SIZE)
			&& IS_ALIGNED(entry->size, XPAGE_SIZE)))
		return false;
	return true;
}

static void make_pages(struct pages *pages) {
	pages->paddr = ALIGN_ABOVE(pages->paddr, PAGE_SIZE);
	pages->size = ALIGN_BELOW(pages->paddr + pages->size,
							  PAGE_SIZE) - pages->paddr;
}

void add_memory_pages(void)
{
	int num = *(int *)0x8000;
	struct e820map_t *e820map = (void *)0x8004;

	extern uint32_t __kern_start, __kern_end;
	addr_t kern_start = premap_addr(&__kern_start);
	addr_t kern_end = premap_addr(&__kern_end);
	struct pages pages;

	for (int i = 0; i < num; i++, e820map++) {

		if (e820map->type != E820_MEMORY)
			continue;

		addr_t mem_end = e820map->base_addr + e820map->length;
		/*kpdebug("%x %x %x %x\n",
				(uint32_t)e820map->base_addr,
				(uint32_t)mem_end,
				(uint32_t)kern_start,
				(uint32_t)kern_end);*/

		if (mem_end < premap_addr(KERN_START))
			continue;
		/* What flags should I set */
		/* Be care about the align */
		if (mem_end < kern_end
				|| kern_end < e820map->base_addr) {
			/* no intersect */
			pages = (struct pages){
				.paddr = e820map->base_addr,
				.size = e820map->length,
				.flags = GFP_UNSAFE
			};
			/*
			 * We use GFP_UNSAFE here because e820map locate on
			 * this memory area, so we cannot erase these data
			 */
			FIXME(set correct flags)

			print_pages(&pages);
			make_pages(&pages);
			if (pages.size)
				free_pages(&pages);
		} else {
			if (kern_end < mem_end) {
				/* have been */
				pages = (struct pages){
					.paddr = kern_end,
					.size = mem_end - kern_end,
					.flags = GFP_UNSAFE
				};
				/*
				 * We use GFP_UNSAFE here to jump pmemset procedure
				 * so that we can get a faster booting speed.
				 */
				FIXME(set correct flags)

				print_pages(&pages);
				make_pages(&pages);
				if (pages.size)
					free_pages(&pages);
			}
			if (e820map->base_addr < kern_start) {
				pages = (struct pages){
					.paddr = e820map->base_addr,
					.size = kern_start - e820map->base_addr,
					.flags = GFP_UNSAFE
				};
				/*
				 * Since two areas above are set to GFP_UNSAFE
				 * so we just set this flag here.
				 */
				FIXME(set correct flags)

				print_pages(&pages);
				make_pages(&pages);
				if (pages.size)
					free_pages(&pages);
			}
		}
	}

	//kputs("\n");
}
