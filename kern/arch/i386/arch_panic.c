/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <aim/panic.h>
#include <arch-irq.h>
#include <arch-mp.h>

#define APIC_DEST_ALLBUT 0xC0000 // Dest shorthand All excluding self
#define NMI_VECTOR 0x02

void lapicw(int, int);

static inline void
send_IPI_shortcut(unsigned int shortcut, int vector, unsigned int dest)
{
	unsigned int cfg = shortcut | dest;
	if (vector == NMI_VECTOR)
		cfg |= 0x400; // Delivery mode set to NMI;

	lapicw(ICRHI, 0);
	/* write low doubleword cause IPI to be sent */
	lapicw(ICRLO, cfg);
}

void panic_other_cpus()
{
	send_IPI_shortcut(APIC_DEST_ALLBUT, NMI_VECTOR, 0);
}

