# AIM #

[AIMv6](https://github.com/davidgao/AIMv6) is a cross-platform operating system for teaching purposes.

This repository maintains a fork version of [AIM-public](https://github.com/davidgao/AIM-public), which is my OS programming assignment.

## Build
### Compiling
```
ARCH=i386 MACH=pc ./configure --prefix=$HOME/AIM --target=i386-pc-elf --host=i386-pc-elf 
                              --with-kern-start=0x100000 --with-mem-size=0x8000000
                              --enable-werror=no --enable-o2=no
```
### Running
```
qemu-system-i386 -serial mon:stdio -hda AIM.img -smp 4 -m 128
```