/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <aim/boot.h>
#include <elf.h>

#define STAGE2_OFFSET_IN_DISK 512 // bootstage2 code offset in disk

extern uint32_t _stage2_start;
extern uint32_t _stage2_end;
extern uint32_t _new_addr;

__noreturn
void bootmain(void)
{
	char *new_code = (char *)&_new_addr;
	memory_cpy(new_code, &mbr, sizeof mbr);

	char *code;
	//uint32_t len = &_stage2_end - &_stage2_start;

	code = (char *)(&_stage2_start);

	readsect(code, 1); // May cause some problem if bootstage2.bin > 512
	//ide_read(code, STAGE2_OFFSET_IN_DISK, 512);

	//volatile uint32_t entry = (uint32_t)&_stage2_start;

	((void(*)(legacy_mbr *))(uint32_t *)code)((void *)new_code);

	BAD_TRAP;
}

#define O2N(symbol) ( \
		(typeof(&symbol))((void *)symbol + (uint32_t)&_new_addr - 0x7c00) \
)

__attribute__ ((section(".stage2")))
void
printsth()
{
	// I refer this code from internet
	char *str = "Hello World!";
	char *vidptr = (char*)0xb8000; // Address of CGA
	unsigned int i = 0;
	unsigned int j = 0;
	while (j < 80 * 25 * 2) {
		vidptr[j] = ' ';
		vidptr[j + 1] = 0x07;
		j = j + 2;
	}
	j = 0;
	while (str[j] != '\0') {
		vidptr[i] = str[j];
		vidptr[i + 1] = 0x07;
		++j;
		i = i + 2;
	}
}

__noreturn __attribute__ ((section(".stage2.entry")))
void 
bootstage2(legacy_mbr *mbr) 
{
	struct elf32hdr *elf;
	struct elf32_phdr *ph; 
	void *pa;

	elf = (struct elf32hdr *)(((uint32_t)&_new_addr) + 0x10000);

	// Find bootable partition
	int active_idx;
	for (active_idx = 0; active_idx < 4; active_idx++)
		if (mbr->partition_record[active_idx].boot_indicator == 0x80)
			break;
	if (active_idx == 4)
		goto _panic;

	uint32_t sector = mbr->partition_record[active_idx].starting_lba;
	readsect(elf, sector);
	uint32_t elf_offset_in_disk = sector * SECTSIZE;

	const uint32_t elf_magic = 0x464c457f;
	uint32_t *p_magic = (void *)elf;
	if (*p_magic != elf_magic)
		goto _panic;

	/* Load each program segment */
	
	ph = (void*)elf + elf->e_phoff;
	for(uint16_t i = 0; i < elf->e_phnum; i++, ph++) {
		/* Scan the program header table, load each segment into memory */
		if(ph->p_type == PT_LOAD) {
			pa = (void*)ph->p_paddr; // What if pa confilct with mbr in physical memory?
			O2N(ide_read)(pa, elf_offset_in_disk + ph->p_offset, ph->p_filesz);
			O2N(memory_set)(pa + ph->p_filesz, 0, ph->p_memsz - ph->p_filesz);
		}
	}
	
	printsth();

	volatile uint32_t entry = elf->e_entry;

	((void(*)(void))entry)();
	
_panic:
	BAD_TRAP;
}
